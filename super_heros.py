avengers = {'Spiderman': (5, 5, 'araignée a quatre pattes'),'Hulk': (7, 4, 'Grand homme vert'),'Agent 13': (2, 3, 'agent 13'),'M Becker': (2, 6, 'expert en graphe'), 'SuperMan': (6,5,'Héro surpuissant')}

def intelligence_moyenne (dico_héros):
    intelligence_moyenne_ = 0
    if len(dico_héros) == 0 :
        return None
    for valeur_hero in dico_héros.values() : 
        intelligence_moyenne_ += valeur_hero[1]
    return intelligence_moyenne_/len(dico_héros)

def kikelplusfort (dico_héros):
    plus_fort_actuel = 0
    nom_plus_fort = ''
    for (nom_hero, donnee_hero) in dico_héros.items():
        if donnee_hero[0] > plus_fort_actuel : 
            plus_fort_actuel = donnee_hero[0]
            nom_plus_fort = nom_hero
    
    return nom_plus_fort 

def combien_de_cretin (dico_héros): 
    nb_cretin = 0
    a = intelligence_moyenne(dico_héros)
    for donnee_hero in dico_héros.values() : 
        if donnee_hero[1] < a : 
            nb_cretin += 1 
    return nb_cretin