import super_heros 

def test_intelligence_moyenne () :
    avengers = {'Spiderman': (5, 5, 'araignée a quatre pattes'),'Hulk': (7, 4, 'Grand homme vert'),'Agent 13': (2, 3, 'agent 13'),'M Becker': (2, 6, 'expert en graphe'), 'Captain America': (6,5,'Héro surpuissant')}
    marvel = {'Thor' : (7, 3, 'Fort'), 'Iron Man' : (7, 5, 'SuperFort'), 'Thanos' : (8, 0, 'Méchant'),'Black Panther' : (7, 2, 'Bon') }
    dico_vide = dict()
    assert super_heros.intelligence_moyenne(avengers)== 4.6
    assert super_heros.intelligence_moyenne(marvel)== 2.5
    assert super_heros.intelligence_moyenne(dico_vide)== None
    assert (super_heros.intelligence_moyenne(avengers)-4.6) == 0

def test_kikelplusfort () : 
    avengers = {'Spiderman': (5, 5, 'araignée a quatre pattes'),'Hulk': (7, 4, 'Grand homme vert'),'Agent 13': (2, 3, 'agent 13'),'M Becker': (2, 6, 'expert en graphe'), 'Captain America': (6,5,'Héro surpuissant')}
    marvel = {'Thor' : (7, 3, 'Fort'), 'Iron Man' : (7, 5, 'SuperFort'), 'Thanos' : (8, 0, 'Méchant'),'Black Panther' : (7, 2, 'Bon') }
    dico_vide = dict()
    assert super_heros.kikelplusfort(avengers)== 'Hulk'
    assert super_heros.kikelplusfort(marvel)== 'Thanos'
    assert super_heros.kikelplusfort(dico_vide)== ''

def test_combien_de_cretin () :
    avengers = {'Spiderman': (5, 5, 'araignée a quatre pattes'),'Hulk': (7, 4, 'Grand homme vert'),'Agent 13': (2, 3, 'agent 13'),'M Becker': (2, 6, 'expert en graphe'), 'Captain America': (6,5,'Héro surpuissant')}
    marvel = {'Thor' : (7, 3, 'Fort'), 'Iron Man' : (7, 5, 'SuperFort'), 'Thanos' : (8, 0, 'Méchant'),'Black Panther' : (7, 2, 'Bon') }
    dico_vide = dict()
    assert super_heros.combien_de_cretin(avengers)== 2
    assert super_heros.combien_de_cretin(marvel)== 2
    assert super_heros.combien_de_cretin(dico_vide)== 0
    