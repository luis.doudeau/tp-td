"""
Permet de modéliser un le_plateau de jeu avec :
    - une matrice qui contient des nombres entiers
    - chaque nombre entier correspond à un item :
    MUR, COULOIR, PERSONNAGE, FANTOME
"""
#from os import posix_fadvise
import matrice
import copy

MUR = 1
COULOIR = 0
PERSONNAGE = 2
FANTOME = 3

NORD = 'z'
OUEST = 'q'
SUD = 's'
EST = 'd'


def init(nom_fichier="./labyrinthe1.txt"):
    """Construit le plateau de jeu de la façon suivante :
        - crée une matrice à partir d'un fichier texte qui contient des COULOIR et MUR
        - met le PERSONNAGE en haut à gauche cad à la position (0, 0)
        - place un FANTOME en bas à droite
    Args:
        nom_fichier (str, optional): chemin vers un fichier csv qui contient COULOIR et MUR.
        Defaults to "./labyrinthe1.txt".

    Returns:
        le plateau de jeu avec les MUR, COULOIR, PERSONNAGE et FANTOME
    """
    new_matrice = matrice.charge_matrice(nom_fichier)
    matrice.set_val(new_matrice,0, 0, 2)
    matrice.set_val(new_matrice, matrice.get_nb_lignes(new_matrice)-1,matrice.get_nb_colonnes(new_matrice)-1,3)
    return new_matrice

#print(init())

def est_sur_le_plateau(le_plateau, position):
    """Indique si la position est bien sur le plateau

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        [boolean]: True si la position est bien sur le plateau
    """
    (nb_ligne, nb_colonne) = position
    if matrice.get_nb_colonnes(le_plateau)-1 >= nb_colonne >= 0 and matrice.get_nb_lignes(le_plateau)-1 >= nb_ligne >= 0 : 
        return True
    else : 
        return False

def get(le_plateau, position):
    """renvoie la valeur de la case qui se trouve à la position donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple d'entiers de la forme (no_ligne, no_colonne)

    Returns:
        int: la valeur de la case qui se trouve à la position donnée ou
        None si la position n'est pas sur le plateau
    """
    (nb_ligne, nb_colonne) = position
    if est_sur_le_plateau(le_plateau,position):
        return matrice.get_val(le_plateau,nb_ligne, nb_colonne)
    else : 
        return None

def est_un_mur(le_plateau, position):
    """détermine s'il y a un mur à la position donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple d'entiers de la forme (no_ligne, no_colonne)

    Returns:
        bool: True si la case à la position donnée est un MUR, False sinon
    """
    if get(le_plateau, position) == 1 : 
        return True
    else : 
        return False

def contient_fantome(le_plateau, position):
    """Détermine s'il y a un fantôme à la position donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        bool: True si la case à la position donnée est un FANTOME, False sinon
    """
    if get(le_plateau, position) == 3 :
        return True
    else : 
        return False

def est_la_sortie(le_plateau, position):
    """Détermine si la position donnée est la sortie
    cad la case en bas à droite du labyrinthe

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        bool: True si la case à la position donnée est la sortie, False sinon
    """
    (nb_ligne, nb_colonne) = position
    if matrice.get_nb_colonnes(le_plateau)-1 == nb_ligne and matrice.get_nb_lignes(le_plateau)-1 == nb_colonne :
        return True
    else : 
        return False

def deplace_personnage(le_plateau, personnage, direction):
    """déplace le PERSONNAGE sur le plateau si le déplacement est valide
    Le personnage ne peut pas sortir du plateau ni traverser les murs
    Si le déplacement n'est pas valide, le personnage reste sur place

    Args:
        le_plateau (plateau): un plateau de jeu
        personnage (tuple): la position du personnage sur le plateau
        direction (str): la direction de déplacement SUD, EST, NORD, OUEST

    Returns:
        [tuple]: la nouvelle position du personnage
    """
    (ligne, colonne) = personnage
    if direction == NORD and le_plateau[ligne-1][colonne] == 0 or le_plateau[ligne-1][colonne] == 3 :
        matrice.set_val(le_plateau,ligne,colonne, 0)
        matrice.set_val(le_plateau,ligne-1,colonne, 2)
        return (ligne-1,colonne) 
    if direction == SUD and le_plateau[ligne+1][colonne] == 0 or le_plateau[ligne+1][colonne] == 3:
        matrice.set_val(le_plateau,ligne,colonne, 0)
        matrice.set_val(le_plateau,ligne+1,colonne, 2)
        return (ligne+1,colonne)
    if direction == EST and le_plateau[ligne][colonne+1] == 0 or le_plateau[ligne][colonne+1] == 3 : 
        matrice.set_val(le_plateau,ligne,colonne, 0)
        matrice.set_val(le_plateau,ligne,colonne+1, 2)
        return (ligne, colonne+1)
    if direction == OUEST and le_plateau[ligne][colonne-1] == 0 or le_plateau[ligne][colonne-1] == 3 :
        matrice.set_val(le_plateau,ligne,colonne, 0)
        matrice.set_val(le_plateau,ligne,colonne-1, 2)
        return (ligne, colonne-1)
    else : 
        return (ligne,colonne)

# print(deplace_personnage(init(), (0,0), 'w'))

def voisins(le_plateau, position):
    """Renvoie l'ensemble des positions cases voisines accessibles de la position renseignées
    Une case accessible est une case qui est sur le plateau et qui n'est pas un mur
    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        set: l'ensemble des positions des cases voisines accessibles
    """
    (ligne, colonne) = position
    ensemble_position = set()
    if est_sur_le_plateau(le_plateau, (ligne-1,colonne)) :  
        if le_plateau[ligne-1][colonne] == 0 or le_plateau[ligne-1][colonne] == 3 : 
            ensemble_position.add((ligne-1,colonne))
    if est_sur_le_plateau(le_plateau, (ligne+1,colonne)) : 
        if le_plateau[ligne+1][colonne] == 0 or le_plateau[ligne+1][colonne] == 3 : 
            ensemble_position.add((ligne+1,colonne))
    if est_sur_le_plateau(le_plateau, (ligne,colonne+1)) :
        if le_plateau[ligne][colonne+1] == 0 or le_plateau[ligne][colonne+1] == 3 :
            ensemble_position.add((ligne, colonne+1))
    if est_sur_le_plateau(le_plateau, (ligne,colonne-1)) :     
        if le_plateau[ligne][colonne-1] == 0 or le_plateau[ligne][colonne-1] == 3 :
            ensemble_position.add((ligne, colonne-1))

    return ensemble_position

# print(voisins(init(), (4,5)))


def voisins_calque(le_plateau, position):
    """Renvoie l'ensemble des positions cases voisines accessibles de la position renseignées
    Une case accessible est une case qui est sur le plateau et qui n'est pas un mur
    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        set: l'ensemble des positions des cases voisines accessibles
    """
    (ligne, colonne) = position
    ensemble_position = set()
    if est_sur_le_plateau(le_plateau, (ligne-1,colonne)) :  
        if le_plateau[ligne-1][colonne] is not None : 
            ensemble_position.add((ligne-1,colonne))
    if est_sur_le_plateau(le_plateau, (ligne+1,colonne)) : 
        if le_plateau[ligne+1][colonne] is not None : 
            ensemble_position.add((ligne+1,colonne))
    if est_sur_le_plateau(le_plateau, (ligne,colonne+1)) :
        if le_plateau[ligne][colonne+1] is not None :
            ensemble_position.add((ligne, colonne+1))
    if est_sur_le_plateau(le_plateau, (ligne,colonne-1)) :     
        if le_plateau[ligne][colonne-1] is not None :
            ensemble_position.add((ligne, colonne-1))

    return ensemble_position

def fabrique_le_calque(le_plateau, position_depart):
    """fabrique le calque d'un labyrinthe en utilisation le principe de l'inondation :
    
    Args:
        le_plateau (plateau): un plateau de jeu
        position_depart (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        matrice: une matrice qui a la taille du plateau dont la case qui se trouve à la
    position_de_depart est à 0 les autres cases contiennent la longueur du
    plus court chemin pour y arriver (les murs et les cases innaccessibles sont à None)
    """
    matrice_calque = []                                                # J'initialise la matrice calque à une liste vide
    for nb_lignes in range(matrice.get_nb_lignes(le_plateau)):         # Je parcours un nombre de fois qu'il ya de ligne du plateau 
        matrice_calque.append([])                                      # J'insere pour chaque ligne de la matrice calque une liste
        for nb_colonnes in range(matrice.get_nb_colonnes(le_plateau)): # Je parcours un nombre de fois qu'il ya de colonne dans le plateau
            matrice_calque[nb_lignes].append(None)                     # J'ajoute la valeur None pour chacune des valeur la ligne : (ligne, colonne) = None 
    (ligne, colonne) = position_depart                   
    matrice.set_val(matrice_calque,ligne,colonne,0)                    # Je met la case de départ à 0 dans la matrice
    ensemble_position_parcourru = {(position_depart)}                  # On créé un ensemble avec toute les positions que l'on aura parcouru   
    valeur_actuelle = 0                                                # represente la valeur de(s) la case que l'on va etudier, cad si les autres cases les ont comme voisins   
    valeur_a_inserer = 1                                               # represente la distance des autres cases à la case de départ: initialiser à 1 car la distance case_depart/voisin = 1
    nb_colonne = 0                                                     # indique la colonne ou l'on se trouve dans la matrice (init à la première colonne)
    nb_ligne = 0     
    check = True                                                  # indique la ligne ou l'on se trouve dans la matrice (init à la première ligne) 
    while check :  # tant que la matrice est modifié, on continuer de la parcourir
        matrice_check = copy.deepcopy(matrice_calque)  
        while nb_ligne < len(matrice_calque) :                         # tant que le numero de la ligne est inferieur au nombre de ligne-1 on continue le parcours
            while nb_colonne < len(matrice_calque[0]) :                # tant que le numéro de la colonne est inférieur au nombre de colonne-1 on continue de parcourir
                nb_colonne += 1                                        
                if not(est_un_mur(le_plateau,(nb_ligne,nb_colonne))) and (nb_ligne,nb_colonne) not in ensemble_position_parcourru :  # si la case n'est pas un mûr et que la position n'est pas une position parcouru : on continue
                    for position_ in ensemble_position_parcourru :   # on parcours l'ensemble des position parcouru 
                        if position_ in voisins_calque(matrice_calque, (nb_ligne, nb_colonne)) and matrice_calque[position_[0]][position_[1]] == valeur_actuelle and 0 <= nb_ligne <= matrice.get_nb_lignes(matrice_calque)-1 and 0 <= nb_colonne <= matrice.get_nb_colonnes(matrice_calque)-1 : # Si la case que l'on parcours est un voisin de la case que l'on etudie et que ce voisin à comme valeur la bonne longueur entre la case etudier et celle parcouru  
                            matrice.set_val(matrice_calque, nb_ligne, nb_colonne, valeur_a_inserer)  # on set la valeur parcouru à : valeur à inserer 
                            ensemble_position_parcourru.add((nb_ligne,nb_colonne)) # on ajoute la position de cette case dans l'ensemble des positions parcouru
                            break  # reprend le parcours de 0 et on reparcours toute la liste
            nb_ligne += 1 
            nb_colonne = -1
            continue  # on continue le parcours pour toutes les lignes restantes
        valeur_actuelle += 1   # ici toute les lignes ont été parcouru, on augmente donc la valeur actuelle de 1 et valeur à inserer de 1
        valeur_a_inserer += 1  # ici, au premier tours de boucle toute les cases voisin de la case de départ ont été modifié dans la matrice
        nb_ligne = -1
        nb_colonne = -1 
        if matrice_check == matrice_calque :  # on test si la matrice à été modifier lors de son parcours
            check = False
        continue # on reprend tout le parcours de la matrice entierement
    return matrice_calque  

#matrice.affiche(fabrique_le_calque(init(),(2,2)))


def fabrique_chemin(le_plateau, position_depart, position_arrivee):
    """Renvoie le plus court chemin entre position_depart position_arrivee

    Args:
        le_plateau (plateau): un plateau de jeu
        position_depart (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 
        position_arrivee (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        list: Une liste de positions entre position_arrivee et position_depart
        qui représente un plus court chemin entre les deux positions
    """
    liste_chemin_plus_court = [(position_arrivee)]
    le_plateau = fabrique_le_calque(init(),position_depart)
    position = position_arrivee
    (ligne,colonne) = position
    cpt = 0
    while (ligne, colonne) != position_depart or cpt <  100 :
        cpt += 1
        for voisin in voisins_calque(le_plateau,(ligne, colonne)) :
            if le_plateau[voisin[0]][voisin[1]] == le_plateau[ligne][colonne]-1 :
                (ligne,colonne) = (voisin[0],voisin[1])
                liste_chemin_plus_court.append((ligne, colonne)) 
    liste_chemin_plus_court.pop(len(liste_chemin_plus_court)-1)
    return liste_chemin_plus_court

# print(fabrique_chemin(init(),(5,3), (8,8)))          

def deplace_fantome(le_plateau, fantome, personnage):
    """déplace le FANTOME sur le plateau vers le personnage en prenant le chemin le plus court
    Args:
        le_plateau (plateau): un plateau de jeu
        fantome (tuple): la position du fantome sur le plateau
        personnage (tuple): la position du personnage sur le plateau

    Returns:
        [tuple]: la nouvelle position du FANTOME
    """
    if fantome == personnage : 
        return fantome
    liste_chemin = list(reversed(fabrique_chemin(le_plateau, fantome, personnage)))
    if liste_chemin[0] == (fantome[0],fantome[1]-1):
        matrice.set_val(le_plateau, liste_chemin[0][0], liste_chemin[0][1],3)
        matrice.set_val(le_plateau, fantome[0], fantome[1],0)

    elif liste_chemin[0] == (fantome[0],fantome[1]+1):
        matrice.set_val(le_plateau, liste_chemin[0][0], liste_chemin[0][1],3)
        matrice.set_val(le_plateau, fantome[0], fantome[1],0)

    elif liste_chemin[0] == (fantome[0]-1,fantome[1]):
        matrice.set_val(le_plateau, liste_chemin[0][0], liste_chemin[0][1],3)
        matrice.set_val(le_plateau, fantome[0], fantome[1],0)

    elif liste_chemin[0] == (fantome[0]+1,fantome[1]):
        matrice.set_val(le_plateau, liste_chemin[0][0], liste_chemin[0][1],3)
        matrice.set_val(le_plateau, fantome[0], fantome[1],0)
    
    return liste_chemin[0]

# print(deplace_fantome(init(),(4,4),(0,0)))