"""Init Dev : TP9"""

# ==========================
# Petites bêtes
# ==========================

pokedex = [("Bulbizarre", "Plante"), ("Aeromite", "Poison"), ("Abo", "Poison")]

def toutes_les_familles(pokedex):
    """détermine l'ensemble des familles représentées dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        set: l'ensemble des familles représentées dans le pokedex
    """
    ensemble_famille = set()
    for (_, famille) in pokedex : 
        if famille not in ensemble_famille :  
            ensemble_famille.add(famille)
    return ensemble_famille

#print(toutes_les_familles(pokedex))

def nombre_pokemons(pokedex, famille):
    """calcule le nombre de pokemons d'une certaine famille dans un pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)
        famille (str): le nom de la famille concernée

    Returns:
        int: le nombre de pokemons d'une certaine famille dans un pokedex
    """
    nombre_famille = 0
    for (_, nom_famille) in pokedex : 
        if nom_famille == famille :
            nombre_famille += 1
    return nombre_famille

#print(nombre_pokemons(pokedex, 'Poison'))

def frequences_famille(pokedex):
    """Construit le dictionnaire de fréqeunces des familles d'un pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str)
        et la valeur associée est le nombre de représentants de la famille (int)
    """
    dico_famille = dict()
    for (_, famille) in pokedex : 
        if famille in dico_famille.keys() :
            dico_famille[famille]+=1
        else : 
            dico_famille[famille] = 1
    return dico_famille

#print(frequences_famille(pokedex))

def dico_par_famille(pokedex):
    """Construit un dictionnaire dont les les clés sont le nom de familles (str)
    et la valeur associée est l'ensemble (set) des noms des pokemons de cette
    famille dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur associée est
        l'ensemble (set) des noms des pokemons de cette famille dans le pokedex
    """
    dico_famille = dict()
    for (pokemon, famille) in pokedex : 
        if famille in dico_famille.keys() : 
            dico_famille[famille].add(pokemon)
        else : 
            dico_famille[famille] = {pokemon}
    return dico_famille

#print(dico_par_famille(pokedex))

def famille_la_plus_representee(pokedex):
    """détermine le nom de la famille la plus représentée dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        str: le nom de la famille la plus représentée dans le pokedex
    """
    max_famille = 0
    nom_famille_la_plus_rpz = ''
    for (famille, liste_pokemon) in dico_par_famille(pokedex).items() : 
        if len(liste_pokemon) > max_famille :  
            max_famille = len(liste_pokemon)
            nom_famille_la_plus_rpz = famille
    return nom_famille_la_plus_rpz

#print(famille_la_plus_representee(pokedex))

# ==========================
# La maison qui rend fou
# ==========================

# Dans 'mqrf1', si on commence par Abribus, on obtient le formulaire au guichet Astus.
# Dans 'mqrf2', si on commence par Abribus, on obtient le formulaire au guichet Saudepus. 

def quel_guichet(mqrf, guichet):
    """Détermine le nom du guichet qui délivre le formulaire A-38

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        str: le nom du guichet qui finit par donner le formulaire A-38
    """
    guichet_actuel = guichet
    i = 0
    while guichet_actuel != None and i <= len(mqrf)  : 
        i += 1
        a = guichet_actuel
        guichet_actuel = mqrf[guichet_actuel]

    return a

def quel_guichet_v2(mqrf, guichet):
    """Détermine le nom du guichet qui délivre le formulaire A-38
    ainsi que le nombre de guichets visités

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        tuple: le nom du guichet qui finit par donner le formulaire A-38 et le nombre de
        guichets visités pour y parvenir
    """
    guichet_actuel = guichet
    i = 0
    while guichet_actuel != None and i <= len(mqrf)  : 
        i += 1
        a = guichet_actuel
        guichet_actuel = mqrf[guichet_actuel]

    return (a,i)

# Si on commence sur le guichet "Abribus", on obiendra le formulaire au guichet nommé "Jeancloddus".

def quel_guichet_v3(mqrf, guichet):
    """Détermine le nom du guichet qui délivre le formulaire A-38
    ainsi que le nombre de guichets visités

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        tuple: le nom du guichet qui finit par donner le formulaire A-38 et le nombre de
        guichets visités pour y parvenir
        S'il n'est pas possible d'obtenir le formulaire en partant du guichet de depart,
        cette fonction renvoie None
    """
    guichet_actuel = guichet
    i = 0
    while guichet_actuel != None and i <= len(mqrf)  : 
        i += 1
        a = guichet_actuel
        guichet_actuel = mqrf[guichet_actuel]

    return (a,i)

# ==========================
# Petites bêtes (la suite)
# ==========================


def toutes_les_familles_v2(pokedex):
    """détermine l'ensemble des familles représentées dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        set: l'ensemble des familles représentées dans le pokedex
    """
    ...

def nombre_pokemons_v2(pokedex, famille):
    """calcule le nombre de pokemons d'une certaine famille dans un pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)
        famille (str): le nom de la famille concernée

    Returns:
        int: le nombre de pokemons d'une certaine famille dans un pokedex
    """
    ...

def frequences_famille_v2(pokedex):
    """Construit le dictionnaire de fréqeunces des familles d'un pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur
        associée est le nombre de représentants de la famille (int)
    """
    ...

def dico_par_famille_v2(pokedex):
    """Construit un dictionnaire dont les les clés sont le nom de familles (str)
    et la valeur associée est l'ensemble (set) des noms des pokemons de
    cette famille dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur associée est
        l'ensemble (set) des noms des pokemons de cette famille dans le pokedex
    """
    ...

def famille_la_plus_representee_v2(pokedex):
    """détermine le nom de la famille la plus représentée dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        str: le nom de la famille la plus représentée dans le pokedex
    """
    ...
