"""TP7 une application complète
    ATTENTION VOUS DEVEZ METTRE DES DOCSTRING A TOUTES VOS FONCTIONS
    """
def afficher_menu(titre,liste_options):
    print('+'+(len(titre)+6)*'-'+'+')
    print('|'+'   '+titre+'   '+'|') 
    print('+'+(len(titre)+6)*'-'+'+')

    for i in range(len(liste_options)):
        print(i+1,'->',liste_options[i])

#afficher_menu('Hello',['Charger un fichier','Rechercher la population d une commune','Afficher la population d un département','Quitter'])

def demander_nombre(message,borne_max):
    print(message)
    rep=input()
    if int(rep)>0 and int(rep)<borne_max and rep.isdecimal() :
        return int(rep) 
    else : 
        return None

#print(demander_nombre("Veuillez choisir un nombre",4))

def menu(titre,liste_options):
    afficher_menu(titre,liste_options)
    rep=demander_nombre("Veuillez choisir un nombre",4)
    print(liste_options[rep])

#menu('Hello',['Charger un fichier','Rechercher la population d une commune','Afficher la population d un département','Quitter'])

'''def programme_principal():
    liste_options=["Charger un fichier","Rechercher la population d'une commune","Afficher la population d'un département","Quitter"]
    liste_communes=[]
    while True:
        rep=menu("MENU DE MON APPLICATION",liste_options)
        if rep is None:
            print("Cette option n'existe pas")
        elif rep==1:
            print("Vous avez choisi",liste_options[rep-1])
        elif rep==2:
            print("Vous avez choisi",liste_options[rep-1])
        elif rep==3:
            print("Vous avez choisi",liste_options[rep-1])
        else:
            break
        input("Appuyer sur Entrée pour continuer")
    print("Merci au revoir!")'''


def charger_fichier_population(nom_fic):
    res=[]
    b=''
    fic=open(nom_fic,"r")
    fic.readline()
    for ligne in fic :
        a=ligne.split(';')
        for nb in a[4]:
            if nb.isdecimal():
                b=b+nb
        res.append((a[0],a[1],b))
        b=''
    return res

#print(charger_fichier_population('extrait1.csv'))

def population_d_une_commune(liste_pop,nom_commune):
    z=charger_fichier_population('extrait1.csv')
    i=0
    while i<len(liste_pop):
        if liste_pop[i][1]==nom_commune :
            return liste_pop[i][2] 
        i+=1
    return None

#print(population_d_une_commune(charger_fichier_population('extrait1.csv'),'Orléans'))

def liste_des_communes_commencant_par(liste_pop,debut_nom):
    i=0
    res=[]
    while i<len(liste_pop):
        if liste_pop[i][1][0:3]==debut_nom:
            res.append(liste_pop[i][1])
        i+=1
    return res

#print(liste_des_communes_commencant_par(charger_fichier_population('extrait1.csv'),'Orl'))

def commune_plus_peuplee_departement(liste_pop, num_dpt):
    max=0
    for elem in liste_pop:
        a=elem[0][0]
        if elem[0][0:2]==num_dpt:
            if max<int(elem[2]):
                max=int(elem[2])
    return max

#print(commune_plus_peuplee_departement(charger_fichier_population('extrait1.csv'),'45'))

def nombre_de_communes_tranche_pop(liste_pop,pop_min,pop_max):
    res=[]
    for ville in liste_pop :
        if int(pop_min)<int(ville[2])<int(pop_max) :
            res.append(ville[1])
    return res

#print(nombre_de_communes_tranche_pop(charger_fichier_population('extrait1.csv'),'1000','100000'))

#def place_top(commune, liste_pop):

def ajouter_trier(commune,liste_pop,taille_max):
    res=liste_pop
    if taille_max>len(liste_pop):
        for i in range (len(liste_pop)) : 
            if int(commune[2])>int(liste_pop[i][2]):
                res.insert(i,commune)
                return res

#print(ajouter_trier(('45260','Lorris','3000'),[('45234', 'Orléans', '119085'), ('45232', 'Olivet', '22474'),('45235', 'Ormes', '4200'),('51204', 'Damery', '1464'), ('77347', 'Les Ormes-sur-Voulzie', '868'),('77345', 'Orly-sur-Morin', '688'), ('45233', 'Ondreville-sur-Essonne', '416'), ('51205', 'Dampierre-au-Temple', '279'),('77348', 'Ormesson', '249'),('51208', 'Dampierre-sur-Moivre', '115'), ('51206', 'Dampierre-le-Château', '108')],15))

def top_n_population(liste_pop,nb):
    ...

def population_par_departement(liste_pop):
    res=[]
    somme=((liste_pop[0][0][0:2],liste_pop[0][2]))
    for i in range (1,len(liste_pop)):
        if liste_pop[i][0][0:2]==somme[0][0:2] :
            somme=(liste_pop[i][0][0:2],int(somme[1])+int(liste_pop[i][2]))
        else :
            res.append(somme) 
            somme=((liste_pop[i][0][0:2],liste_pop[i][2]))
    res.append(somme)        
    return res

#print(population_par_departement(charger_fichier_population('population2017.csv')))

def sauve_population_dpt(nom_fic,liste_pop_dep):
    fic=open(nom_fic,"w")
    for elem in liste_pop_dep :
        ligne=elem[0]+','+str(elem[1])+'\n'
        fic.write(ligne)
    fic.close()

#print(sauve_population_dpt('FichierCree.csv',population_par_departement(charger_fichier_population('population2017.csv'))))

# appel au programme principal
#programme_principal() 


def pop_france_totale (liste_pop_dep): # Fonction Bonus
    res=0
    for dep in liste_pop_dep : 
        res+=dep[1]

    return res

#print(pop_france_totale(population_par_departement(charger_fichier_population('population2017.csv'))))
