# Codé par Papy Force X, jeune padawan de l'informatique

# Exercice 1 :

# 1.1 : La fonction ne respecte pas ces bonnes pratiques : 
# - Chaque fonction doit réaliser une et une seule tâche et son code fait 20 lignes maximum, sauf dans des cas exceptionnels.
# - Chaque fonction doit, dans la mesure du possible, être testée.

def longueur_ok(mot_de_passe): # je vérifie la longueur
    longueur_ok = 0
    if len(mot_de_passe) < 8:
        longueur_ok = False
    else:
        longueur_ok = True
    return longueur_ok
        
def chiffre_ok (mot_de_passe): # je vérifie s'il y a un chiffre
    chiffre_ok = False
    for lettre in mot_de_passe:
        if lettre.isdigit():
            chiffre_ok = True
    return chiffre_ok
    

def sans_espace (mot_de_passe): # je vérifie qu'il n'y a pas d'espace
        sans_espace = True
        for lettre in mot_de_passe:
            if lettre == " ":
                sans_espace = False
        return sans_espace

def verif_mdp (): # Je gère l'affichage
    login = input("Entrez votre nom : ")
    mot_de_passe_correct = False
    while not mot_de_passe_correct :
        mot_de_passe = input("Entrez votre mot de passe : ")
        if not longueur_ok(login):
            print("Votre mot de passe doit comporter au moins 8 caractères")
        elif not chiffre_ok(login):
            print("Votre mot de passe doit comporter au moins un chiffre")
        elif not sans_espace(login):
            print("Votre mot de passe ne doit pas comporter d'espace")	   
        else:
            mot_de_passe_correct = True        
    print("Votre mot de passe est correct")
    return mot_de_passe

verif_mdp()


# Exercice 2 :

def longueur_ok(mot_de_passe): # je vérifie la longueur
    longueur_ok = 0
    if len(mot_de_passe) < 8:
        longueur_ok = False
    else:
        longueur_ok = True
    return longueur_ok
        
def chiffre_ok (mot_de_passe): # je vérifie s'il y a un chiffre
    chiffre_ok = False
    cpt_chiffre = 0
    for lettre in mot_de_passe:
        if lettre.isdigit():
            cpt_chiffre+=1
    if cpt_chiffre >= 3 :
        chiffre_ok = True
    return chiffre_ok
    
def chiffre_consec (mot_de_passe): # je vérifie qu'il n'y a pas dans le mdp, deux chiffres consécutifs
    chiffre_consec_ok = True
    for i in range (1,len(mot_de_passe)):
        if mot_de_passe[i].isdigit() and mot_de_passe[i-1].isdigit():
            chiffre_consec_ok = False
    return chiffre_consec_ok

def rep_plus_petit_chiffre (mot_de_passe): # je verifie que le plus petit chiffre du mdp n'apparait qu'une seule fois
    liste_chiffre = []
    rep_plus_petit_chiffre = True
    for i in range (len(mot_de_passe)): 
        if mot_de_passe[i].isdigit():
            liste_chiffre.append(mot_de_passe[i])
    if liste_chiffre.count(min(liste_chiffre))>1 : 
        rep_plus_petit_chiffre = False
    return rep_plus_petit_chiffre

def sans_espace (mot_de_passe): # je vérifie qu'il n'y a pas d'espace
        sans_espace = True
        for lettre in mot_de_passe:
            if lettre == " ":
                sans_espace = False
        return sans_espace

def verif_mdp (): # Je gère l'affichage
    nom_utilisateur = input("Entrez votre nom")
    login = input("Entrez votre nom : ")
    mot_de_passe_correct = False
    while not mot_de_passe_correct :
        mot_de_passe = input("Entrez votre mot de passe : ")
        if not longueur_ok(login):
            print("Votre mot de passe doit comporter au moins 8 caractères")
        elif not chiffre_ok(login):
            print("Votre mot de passe doit comporter au moins un chiffre")
        elif not sans_espace(login):
            print("Votre mot de passe ne doit pas comporter d'espace")
        elif not chiffre_consec(login):
            print("Votre mot de passe ne doit pas comporter deux chiffres consécutifs")
        elif not rep_plus_petit_chiffre(login):
            print("Le chiffre le plus petit doit apparaître une seule fois")	   
        else:
            mot_de_passe_correct = True        
    print("Votre mot de passe est correct")
    return mot_de_passe

verif_mdp()

# Exercice 3 : 

def charger_mdp (nom_utilisateur):
    fichier = open("mdpUltraSecret.txt", "r")
    fichier.write([nom_utilisateur,verif_mdp()])
    print(fichier.read())

# Exercice 4 : 

def ajout_update_mdp (nom_utlisateur):
    fichier = open("mdpUltraSecret.txt", "r")
    for mdp in fichier : 
        if mdp[0] == nom_utlisateur : 
            mdp[1] = verif_mdp()
     