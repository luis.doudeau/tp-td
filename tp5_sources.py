def mystere(liste,valeur):
    """[Ce programme renvoie soit : le nombre de fois ou valeur n'a pas été égale à l'élement actuelle dans la liste (jusqu'à ce qu'au return) 
    si il ya eu plus de 3 fois la valeur dans la liste. Soit None si il ya eu moins de 4 fois la valeur dans la liste, jusqu'a la fin de la liste. 

    Args:
        liste ([list]): [liste d'élement dans une liste, ici ce sont des valeurs de type int]
        valeur ([int]): [valeur de type int (entier), cette valeur sera  ensuite comparé a tout les élements de la liste]
    Returns:
        [xxx]: [le programme renvoie xxx lorqu'il ya exactement 4 valeurs dans la liste égale à valeur]
        [None]: [le programme renvoie None lorque la boucle parcouru toute la liste et n'a pas trouver plus de 3 valeurs égale à valeur]
    """
    xxx=0
    yyy=0
    for elem in liste:
        if elem==valeur:
            yyy+=1
            if yyy>3:
                return xxx
        xxx+=1
    return None

mystere([12,5,8,48,12,418,185,17,5,87],20)

# --------------------------------------
# Exemple de villes avec leur population
# --------------------------------------
liste_villes=["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"]
population=[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]

#---------------------------------------
# Exemple de scores
#---------------------------------------
scores=[352100,325410,312785,220199,127853]
joueurs=['Batman','Robin','Batman','Joker','Batman']

# Question 1 : xxx : contient le nombre de fois ou la valeur n'est pas égale à l'élement en question dans la liste.
#             yyy : contient le nombre de fois ou la valeur est égale a lélement en question dans la liste.
# Question 2 :  return xxx est éxectuter lorsqu'il ya exactement 4 valeurs dans la listes égale à valeur.
# Question 3 : 

def mystere(liste,valeur):
    xxx=0
    yyy=0
    for i in range(len(liste)):
        if liste[i]==valeur:
            yyy+=1
            if yyy>3:
                return xxx
        xxx+=1
    return None

mystere([12,5,8,48,12,418,185,17,5,87],20)

def recherche_indice_nb (liste): 
    res=0
    nb='0123456789'
    for i in range(len(liste)):
        if liste[i] in nb : 
            res=i
            return res

assert(recherche_indice_nb("on est le 30/09/2021" ))==10

def ville_pop (liste_villes,population,ville):
    res=''
    p=0
    for i in range(len(liste_villes)):
        if liste_villes[i] == ville:
            p=i
    if ville not in liste_villes:
        return None

    return population[p]

assert(ville_pop(["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"],[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725],"Tours"))==136463
assert(ville_pop(["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"],[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725],"Orléans"))==116238
assert(ville_pop(["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"],[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725],"Tous"))==None
liste_villes=["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"]
population=[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]


def dep_seuil (liste,seuil):
    res=0
    for i in liste:
        res+=i
    if res > seuil :
        res=True
    elif res<=seuil : 
        res= False
    
    return res

#assert(dep_seuil([1,4,1,2,3,4], 6))==True
assert(dep_seuil([1,4],6))==False

def mail (email):
    i_a=0
    res=0
    nb_arobase=0
    point_apres_arobase = 0
    for y in range(len(email)):
            if email[y] == '@':
                i_a= y
    for i in range(len(email)) :
        if i == 0 and email[i]== '@' :
            res=False
            return res
        if email[i] == ' ' :
            res= False
            return res

        if email[i]=='@':
            nb_arobase+=1
        
        if i == len(email)-1 and email[i]=='.':
            res=False
            return False
        
    for p in range(i_a,len(email)):
        if email[p]=='.':
            point_apres_arobase=True

    if point_apres_arobase!= True :
        res=False
        return False
    
    if nb_arobase>1:
        res=False
        return False
    
    else :
        res=True
    return res

assert(mail("luis.doudeau@gmail.com"))==True
assert(mail("luis.do udeau@gmail.com"))==False
assert(mail("@luis.doudeau@gmail.com"))==False
assert(mail("luis.doudeau@gmailcom"))==False
assert(mail("luis.doudeau@gmail.com."))==False
assert(mail("luis.do@udeau@gmail.com"))==False

def meilleur_score (scores,joueurs,joueur_choisie):
    indice_joueur=[]
    res=[]
    for i in range ((len(joueurs))):
        if joueurs[i]==joueur_choisie :
            indice_joueur.append(i)
    for y in range(len(scores)):
        for x in indice_joueur :
            if y==x :
                res.append(scores[y])

    return res

def meilleur_score2 (scores,joueurs,joueur_choisie):
    res=[]
    i=-1
    for y in range(len(scores)):
        i=i+1
        if joueur_choisie==joueurs[i] :
            res.append(scores[y])
        if joueur_choisie not in joueurs : 
            return None

    return res
    
assert(meilleur_score([352100,325410,312785,220199,127853],['Batman','Robin','Batman','Joker','Batman'],'Batman'))==[352100, 312785, 127853]
assert(meilleur_score2([352100,325410,312785,220199,127853],['Batman','Robin','Batman','Joker','Batman'],'Batman'))==[352100, 312785, 127853]
assert(meilleur_score([352100,325410,312785,220199,127853],['Batman','Robin','Batman','Joker','Batman'],'Joker'))==[220199]
assert(meilleur_score2([352100,325410,312785,220199,127853],['Batman','Robin','Batman','Joker','Batman'],'Joker'))==[220199]
assert(meilleur_score2([352100,325410,312785,220199,127853],['Batman','Robin','Batman','Joker','Batman'],'Luis'))==None


def tri_score (scores):
    x=0
    res=0
    for i in range(1,len(scores)):
        if scores[x]>scores[i]:
            res=True
            x+=1
        else : 
            res=False
            return False
    if len(scores)==0:
        return None
    
    return res

assert(tri_score([352100,325410,312785,220199,127853]))==True
assert(tri_score([352100,325410,127853,312785,220199,127853]))==False
assert(tri_score([]))==None
 
def joueur_meilleur_score (joueurs,joueur_choisie):
    res=0
    for i in joueurs:
        if i == joueur_choisie : 
            res+=1
    return res

assert(joueur_meilleur_score(['Batman','Robin','Batman','Joker','Batman'],'Batman'))==3


def meilleur_score3 (scores,joueurs,joueur_choisie):
    res=[]
    i=-1
    for y in range(len(scores)):
        i=i+1
        if joueur_choisie==joueurs[i] :
            res.append(scores[y])
        if joueur_choisie not in joueurs : 
            return None

    return max(res)

assert(meilleur_score3([352100,325410,312785,220199,127853],['Batman','Robin','Batman','Joker','Batman'],'Batman'))==352100
assert(meilleur_score3([352100,325410,312785,220199,127853],['Batman','Robin','Batman','Joker','Batman'],'Joker'))==220199
assert(meilleur_score3([352100,325410,312785,220199,127853],['Batman','Robin','Batman','Joker','Batman'],'Robin'))==325410

def inserte (liste_triee,score) :
    indice_ou_insert=0
    x=-1
    for i in range(1,(len(liste_triee))) :
        x+=1
        if liste_triee[i]<score and liste_triee[x]>score:
            indice_ou_insert=x+1
        if score>liste_triee[0]:
            indice_ou_insert=0
            return indice_ou_insert
        if score<liste_triee[len(liste_triee)-1] : 
            indice_ou_insert= len(liste_triee)
    return indice_ou_insert

assert(inserte([352100,325410,312785,220199,127853],326000))==1
assert(inserte([352100,325410,312785,220199,127853],1))==5
assert(inserte([352100,325410,312785,220199,127853],355000))==0
assert(inserte([352100,325410,312785,220199,127853],128000))==4

def new_score(score,joueurs,liste_new_scores,new_joueurs):
    b=[]
    x=-1
    g=-1
    for y in range(len(liste_new_scores)):
        x+=1
        g=-1
        b=[]
        if liste_new_scores[y]>score[0]:
            b.append(0)
        if liste_new_scores[y]<score[len(score)-1] : 
            b.append(len(score))
        elif liste_new_scores[y]<=score[0] and liste_new_scores[y]>=score[len(score)-1] :
            for k in range(1,len(score)): 
                g+=1
                if liste_new_scores[y]>score[k] and liste_new_scores[y]<score[g]:
                    b.append(k)
        for z in range(len(b)):
            score.insert(b[z],liste_new_scores[y])
        
        for r in range(len(b)):
            joueurs.insert(b[z],new_joueurs[y])
            z=z-1
    
    return (score,joueurs)

print(new_score([352100,325410,312785,220199,127853],['Batman','Robin','Batman','Joker','Batman'],[650000,5600,340000],['Luis','Thomas','David']))

