"""Init Dev : TP10"""

# =====================================================================
# Exercice 1 : Choix de modélisation et complexité
# =====================================================================
# Modélisation n°1
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v1 dans le fichier de tests

# 1.1) Le pokedex de romain serai implémanté par : pokedex_anakin_v1.add(mon_pokedex)
#                                                  pokedex_anakin_v1[nom_pokemon]={attaque_de_mon_pokemon_1, attaque_de_mon_pokemon_2, ...}

def appartient_v1(pokemon, pokedex): 
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    for (nom_pokemon, _ ) in pokedex : 
        if nom_pokemon == pokemon :
            return True
    return False

def toutes_les_attaques_v1(pokemon, pokedex): 
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    ensemble_attaque = set()
    for (nom_pokemon, nom_attaque) in pokedex : 
        if nom_pokemon == pokemon and nom_attaque not in ensemble_attaque : 
            ensemble_attaque.add(nom_attaque)
    return ensemble_attaque


def nombre_de_v1(attaque, pokedex): 
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    nb_pokemon_avec_cette_attaque = 0
    for (nom_pokemon, nom_attaque) in pokedex : 
        if nom_attaque == attaque : 
            nb_pokemon_avec_cette_attaque += 1
    return nb_pokemon_avec_cette_attaque

pokedex_romain = {('Maraiste', 'Eau'), ('Maraiste','Sol'),('Racaillou','Sol'), ('Racaillou','Roche')}

pokedex_anakin = {('Carmache', 'Dragon'), ('Carmache', 'Sol'),('Colimucus', 'Dragon'), ('Palkia', 'Dragon'),('Palkia', 'Eau')}

def dico_freq (pokedex) : 
    dico_freq = dict()
    for (_, nom_attaque) in pokedex : 
        if nom_attaque not in dico_freq.keys():
            dico_freq[nom_attaque]= 1
        else : 
            dico_freq[nom_attaque]+= 1
    return dico_freq

def attaque_preferee_v1(pokedex):
    """
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    dico = dico_freq(pokedex)
    attaque_pref = None
    nombre_appartition_max = 0
    for (nom_attaque, nombre_appartition) in dico.items():
        if nombre_appartition > nombre_appartition_max : 
            nombre_appartition_max = nombre_appartition
            attaque_pref = nom_attaque
    return attaque_pref

# =====================================================================
# Modélisation n°2
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v2 dans le fichier de tests

def appartient_v2(pokemon, pokedex):
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    return pokemon in pokedex.keys()

def toutes_les_attaques_v2(pokemon, pokedex):
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    return pokedex[pokemon]   


def nombre_de_v2(attaque, pokedex):
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    nb_pokemon_utilisant_cette_attaque = 0
    for (_, ses_attaques) in pokedex.items() :
        if attaque in ses_attaques :
            nb_pokemon_utilisant_cette_attaque += 1
    return nb_pokemon_utilisant_cette_attaque

pokedex_anakinn = {'Maraiste' : {'Eau','Sol'}, 'Racaillou' : {'Sol','Roche'}}

def dico_freq_v2 (pokedex) : 
    dico_freq = dict()
    for nom_attaque in pokedex.values() :
        for attaque in nom_attaque : 
            if attaque not in dico_freq.keys():
                dico_freq[attaque]= 1
            else :
                dico_freq[attaque]+= 1
    return dico_freq

def attaque_preferee_v2(pokedex):
    """
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    dico = dico_freq_v2(pokedex)
    attaque_pref = None
    nombre_appartition_max = None
    for (nom_attaque, nombre_appartition) in dico.items():
        if nombre_appartition_max is None or nombre_appartition > nombre_appartition_max : 
            nombre_appartition_max = nombre_appartition
            attaque_pref = nom_attaque
    return attaque_pref

# =====================================================================
# Modélisation n°3
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v3 dans le fichier de tests


def appartient_v3(pokemon, pokedex):
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    for nom_des_pokemons in pokedex.values() :
        for nom_pokemon in nom_des_pokemons : 
            if pokemon in nom_pokemon :
                return True 
    return False

def toutes_les_attaques_v3(pokemon, pokedex):
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon passé en paramètre
    """
    ensemble_attaque_du_pokemon = set()
    for (nom_attaque, nom_pokemons) in pokedex.items() : 
        if pokemon in nom_pokemons : 
            ensemble_attaque_du_pokemon.add(nom_attaque)
    return ensemble_attaque_du_pokemon

def nombre_de_v3(attaque, pokedex):
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    if attaque in pokedex.keys() :
        return len(pokedex[attaque])
    else : 
        return 0

def attaque_preferee_v3(pokedex):
    """
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    max_attaque_presente = None
    attaque_la_plus_presente = None
    for (nom_attaque, les_attaques) in pokedex.items() : 
        if  max_attaque_presente is None  or len(les_attaques) > max_attaque_presente: 
            max_attaque_presente = len(les_attaques) 
            attaque_la_plus_presente = nom_attaque

    return attaque_la_plus_presente

pokedex_romainnn = { 'Eau': {'Maraiste'}, 'Sol' : {'Maraiste','Racaillou'}, 'Roche' : {'Racaillou'}}


# =====================================================================
# Transformations
# =====================================================================

# Version 1 ==> Version 2

def v1_to_v2(pokedex_v1):
    """
    param: prend en paramètre un pokedex version 1
    renvoie le même pokedex mais en version 2
    """
    res = dict()
    for (nom_pokemon, nom_attaque) in pokedex_v1 : 
        if nom_pokemon not in res.keys() : 
            res[nom_pokemon] = {nom_attaque}
        else : 
            res[nom_pokemon].add(nom_attaque)
    return res 

# Version 1 ==> Version 2

def v2_to_v3(pokedex_v2):
    """
    param: prend en paramètre un pokedex version2
    renvoie le même pokedex mais en version3
    """
    res = dict()
    for (nom_pokemon, nom_attaque) in pokedex_v2.items() : 
        for attaque in nom_attaque :
            if attaque not in res.keys() : 
                res[attaque] = {nom_pokemon}
            else : 
                res[attaque].add(nom_pokemon)
    return res 

# =====================================================================
# Exercice 2 : Ecosystème
# =====================================================================

ecosysteme_1 = { 'Loup': 'Mouton', 'Mouton':'Herbe', 'Dragon':'Lion', 'Lion':'Lapin', 'Herbe':None, 'Lapin':'Carotte', 'Requin':'Surfer'}

def extinction_immediate(ecosysteme, animal):
    """
    renvoie True si animal s'éteint immédiatement dans l'écosystème faute
    de nourriture
    """
    if animal in ecosysteme.keys():
        if ecosysteme[animal] not in ecosysteme.keys() :
            return True
        else : 
            return False
    
    else : 
        return None

def en_voie_disparition(ecosysteme, animal):
    """
    renvoie True si animal s'éteint est voué à disparaitre à long terme
    """
    cpt = 0
    animal_en_cours = animal
    while cpt < len(ecosysteme) : 
        if animal_en_cours in ecosysteme.keys() : 
            animal_en_cours = ecosysteme[animal]
        else : 
            return True
        cpt += 1
    return False

print(en_voie_disparition(ecosysteme_1, 'Lion'))

def animaux_en_danger(ecosysteme):
    """ renvoie l'ensemble des animaux qui sont en danger d'extinction immédiate"""
    ...


def especes_en_voie_disparition(ecosysteme):
    """ renvoie l'ensemble des animaux qui sont en voués à disparaitre à long terme
    """
    ...