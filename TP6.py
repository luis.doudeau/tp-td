# --------------------------------------
# DONNEES
# --------------------------------------

# exemple de liste d'oiseaux observables


oiseaux=[
        ("Merle","Turtidé"), ("Mésange","Passereau"), ("Moineau","Passereau"), 
        ("Pic vert","Picidae"), ("Pie","Corvidé"), ("Pinson","Passereau"),
        ("Tourterelle","Colombidé"), ("Rouge-gorge","Passereau")
        ]
# exemples de listes de comptage ces listes ont la même longueur que oiseaux
comptage1=[2,0,5,1,2,0,5,3]
comptage2=[2,1,3,0,0,3,5,1]
comptage3=[0,4,0,3,2,1,4,2]

# exemples de listes d'observations. Notez que chaque liste correspond à la liste de comptage de
# même numéro
observations1=[
        ("Merle",2),  ("Moineau",5), ("Pic vert",1), ("Pie",2), 
        ("Tourterelle",5), ("Rouge-gorge",3)
            ]

observations2=[
        ("Merle",2), ("Mésange",1), ("Moineau",3), 
        ("Pinson",3),("Tourterelle",5), ("Rouge-gorge",1)
            ]

observations3=[
        ("Mésange",4),("Pic vert",2), ("Pie",2), ("Pinson",1),
        ("Tourterelle",4), ("Rouge-gorge",2)
            ]

# --------------------------------------
# FONCTIONS
# --------------------------------------

def oiseau_le_plus_observe(liste_observations):
    """ recherche le nom de l'oiseau le plus observé de la liste (si il y en a plusieur on donne le 1er trouve)

    Args:
        liste_observations (list): une liste de tuples (nom_oiseau,nb_observes)

    Returns:
        str: l'oiseau le plus observé (None si la liste est vide)
    """
    oiseau_max=(0,0)
    for i in range(len(liste_observations)):
        if liste_observations[i][1]>oiseau_max[1]:
            oiseau_max=liste_observations[i]
    if oiseau_max == (0,0):
        oiseau_max=None
    return oiseau_max[0]

assert(oiseau_le_plus_observe(observations1))=="Moineau"
assert(oiseau_le_plus_observe(observations2))=="Tourterelle"
assert(oiseau_le_plus_observe(observations3))=="Mésange"
    


#--------------------------------------
# PROGRAMME PRINCIPAL
#--------------------------------------

#afficher_graphique_observation(construire_liste_observations(oiseaux,comptage3))
#comptage=saisie_observations(oiseaux)
#afficher_graphique_observation(comptage)
#afficher_observations(comptage,oiseaux)

# Question 1 : Il yavait un probleme d'indices et de return 


#Exercice 2 : 

def find_carac (oiseaux, oiseau_choisie):
    for i in range(len(oiseaux)):
        if oiseau_choisie==oiseaux[i][0]:
            return oiseaux[i][1] 

assert(find_carac(oiseaux,"Moineau"))=='Passereau'

def same_familly (oiseaux,espece): 
    res=[]
    for i in range(len(oiseaux)):
        if oiseaux[i][1]==espece:
            res.append(oiseaux[i][0])
    return res

assert(same_familly(oiseaux,"Passereau"))==['Mésange', 'Moineau', 'Pinson', 'Rouge-gorge']
assert(same_familly(oiseaux,"Turtidé"))==['Merle']
assert(same_familly(oiseaux,"Corvidé"))==['Pie']

def test_obs (liste_observations): 
    for i in range(1,len(liste_observations)):
        if liste_observations[i]< liste_observations[i-1]:
            return False

        if liste_observations[i][1]==0 : 
            return False 

    return True

assert(test_obs([("Merle",2),  ("Moineau",5), ("Pic vert",1), ("Pie",2), ("Tourterelle",5), ("Rouge-gorge",3)]))==False
assert(test_obs([("Merle",2),  ("Moineau",5), ("Pic vert",1), ("Pie",2),  ("Rouge-gorge",3), ("Tourterelle",5)]))==True
assert(test_obs([("Merle",2),  ("Moineau",5), ("Pic vert",1), ("Pie",2),  ("Youge-gorge",3), ("Tourterelle",5)]))==False
assert(test_obs([("Merle",2),  ("Moineau",5), ("Pic vert",1), ("Pie",2),  ("Rouge-gorge",0), ("Tourterelle",5)]))==False

def specimen_le_plus_observe(liste_observations):
    """ recherche le nom de l'oiseau le plus observé de la liste (si il y en a plusieur on donne le 1er trouve)

    Args:
        liste_observations (list): une liste de tuples (nom_oiseau,nb_observes)

    Returns:
        str: l'oiseau le plus observé (None si la liste est vide)
    """
    oiseau_max=(0,0)
    for i in range(len(liste_observations)):
        if liste_observations[i][1]>oiseau_max[1]:
            oiseau_max=liste_observations[i]
    if oiseau_max == (0,0):
        oiseau_max=None
    return oiseau_max[0]

assert(specimen_le_plus_observe(observations1))=="Moineau"
assert(specimen_le_plus_observe(observations2))=="Tourterelle"
assert(specimen_le_plus_observe(observations3))=="Mésange"

def moyenne (liste_observation):
    res=0
    for elem in liste_observation :
        res+=elem[1]
    
    return res/len(liste_observation)

assert(moyenne(observations3))==2.5
assert(moyenne(observations2))==2.5
assert(moyenne(observations1))==3

def total_fam (liste_observation, liste_oiseaux,famille):
    res=0
    a=[]
    for y in liste_oiseaux:
            if y[1] == famille:
                a.append(y[0])
    for i in range (len(liste_observation)):
        if liste_observation[i][0] in a:
                res+=liste_observation[i][1]
                
    return res

assert(total_fam(observations1,oiseaux,'Passereau'))==8
assert(total_fam(observations1,oiseaux,'Corvidé'))==2
assert(total_fam(observations2,oiseaux,'Passereau'))==8
assert(total_fam(observations3,oiseaux,'Colombidé'))==4

def liste_obs (liste_oiseaux,liste_comptage):
    res=[]
    y=0
    for i in range(len(liste_oiseaux)):
        res.append((liste_oiseaux[i][0],liste_comptage[y]))
        y+=1
    return res

assert(liste_obs(oiseaux,[2,4,7,9,3,1,8,3]))==[('Merle', 2), ('Mésange', 4), ('Moineau', 7), ('Pic vert', 9), ('Pie', 3), ('Pinson', 1), ('Tourterelle', 8), ('Rouge-gorge', 3)]

def liste_obs_manuelle (liste_oiseaux):
    res=[]
    y=0
    for i in range(len(liste_oiseaux)):
        a= input()
        res.append((liste_oiseaux[i][0],a))
        y+=1
    return res

#print(liste_obs_manuelle(oiseaux))


observations4=[("Merle",2), ("Mésange",1), ("Moineau",3), ("Pic Vert",8),("Pie",6),("Pinson",3),("Tourterelle",5), ("Rouge-gorge",3)]

def affiche_liste (liste_oiseaux,liste_obs):
    res=[]
    l=[]
    for i in range (len(liste_obs)):
        for y in range (len(liste_oiseaux)):
            if liste_obs[i][0]==liste_oiseaux[y][0]:
                l.append((liste_obs[i][0],liste_obs[i][1],liste_oiseaux[y][1]))

    for o in range(len(l)):
        a=l[o][0]
        b=l[o][2]
        c=l[o][1]
        for ligne in range(1): 
            print('Nom :',a.ljust(11),'Famille :',b.ljust(10),'Nb Observés :',c)

#print(affiche_liste(oiseaux,observations4))
    
    
def graphe (liste_obs):
    tree_first_letter=[]
    for y in range (len(liste_obs)):
        tree_first_letter.append(liste_obs[y][0][0:3])
    e=[]
    for u in liste_obs : 
        e.append(u[1])
    max_liste_obs=max(e)
    
    for i in range (max_liste_obs-1):
        max_liste_obs-=1
        for x in range(len(liste_obs)):
            if liste_obs[x][1]>=max_liste_obs : 
                print(''.ljust((x*3)+x),'**')
                
    for m in range (len(liste_obs)):
        print(tree_first_letter[m].ljust(2))

            

print(graphe(observations4))
